var express = require('express'),
 app = express(),
 cors = require('cors'),
 port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(cors());

var path = require('path');

var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/local";

var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=50c5ea68e4b0a97d668bc84a";
var clienteMlab = requestjson.createClient(urlmovimientosMlab);

var pg = require('pg');
//var urlUsuarios = "postgres://docker:docker@serverpostgre:5432/bdseguridad";
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad";
var clientePostgre = new pg.Client(urlUsuarios);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.post('/loginPostgre', function(req, res) {
  clientePostgre.connect();
  // Asumo que recibo req.body = {usuario:xxxx,password:yyyy}
  console.log(req.body.email);
  console.log(req.body.password);
  const query = clientePostgre.query('SELECT COUNT(*) FROM usuarios WHERE email=$1 AND password=$2;', [req.body.email, req.body.password], (err, result) => {
    if (err) {
      console.log(err);
    }
    else {
      if (result.rows[0].count >= 1) {
        res.send("Cliente valido");
      }
      else {
        res.send("Cliente invalido");
      }
      //console.log(result.rows[0]);
      //res.send(result.rows[0]);
    }
  });
});

app.post('/login', function(req, res) {
 //res.sendFile(path.join(__dirname, 'index.html'));
   console.log(req.body);
   var usuario = req.body.email;
   var password = req.body.password;
   console.log(email);
   console.log(password);
   if ((email == 'micorreo@postal.es') && (password == 'mipassword')) {
     res.send("Cliente valido");
   } else {
     res.send("Cliente invalido");
   }
});

app.get('/movimientos', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    mongoClient.connect(url, function(err, db) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        console.log("Connected successfully to server");
        var col = db.collection('movimientos');
        col.find({}).limit(3).toArray(function(err, docs) {
          res.send(docs);
      });
      db.close();
    }
    })
});

app.post('/movimientos2', function(req, res) {

 mongoClient.connect(url, function(err, db) {
   var col = db.collection('movimientos');
   console.log("Traying to intert to server");
   db.collection('movimientos').insertOne({a:1}, function(err, r) {
     console.log(r.insertedCount + ' registros insertados');
   });

   db.collection('movimientos').insertMany([{a:1}, {a:3}], function(err, r) {
     console.log(r.insertedCount + ' registros insertados');
   });

   db.collection('movimientos').insertOne(req.body, function(err, r) {
     console.log(r.insertedCount + ' registros insertados');
   });

   db.close();
   res.send("ok");
 })
});

app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('&f={"idcliente":1, "nombre": 1, "apellidos": 1}', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/movimientos', function(req, res) {
  /* Crear movimiento en MLab */

});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
})
